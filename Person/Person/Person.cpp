#include "Person.h"


// properties are initialized here using a "constructor initializer list"
Person::Person(const string &name, unsigned short age): _name(name), _age(age) { }


void Person::setName(const string &name) {
    _name = name;
}


void Person::setAge(unsigned short age) {
    _age = age;
}


void Person::growOlder() {
    _age++;
}


void Person::addChild(const Person& child) {
    _children.push_back(child);
}


const string& Person::getName() const {
    return _name;
}


unsigned short Person::getAge() const {
    return _age;
}


const vector<Person>& Person::getChildren() const {
    return _children;
}


ostream& operator<< (ostream& output, const Person& person) {
    output << "Name: " << person._name << ", " << "Age: " << person._age;
    if (person._children.size() > 0) {
        output << ", " << "Children: " << person._children.size();
    }
    return output;
}
