/**
 * @file Person.h
 * @author Brian R. Snider
 */

#ifndef PERSON_H
#define PERSON_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;


/**
 * This class implements a simple Person object with a name and an age.
 */
class Person {

  public:

    // constructors

    /**
     * Create a Person with the specified name and age
     *
     * @param name name of the person
     * @param age age of the person, in years
     */
    Person(const string& name, unsigned short age = 0);


    // mutators

    /**
     * Set the name attribute
     *
     * @param name name of the person
     */
    void setName(const string& name);


    /**
     * Set the age attribute
     *
     * @param age age of the person, in years
     */
    void setAge(unsigned short age);


    /**
     * Increment the age attribute by 1 year
     */
    void growOlder();


    /**
     * Add another Person to this person's list of children
     *
     * @param child Person to add as a child
     */
    void addChild(const Person& child);


    // accessors

    /**
     * Get the name of the person
     *
     * @return the name of the person
     */
    const string& getName() const;


    /**
     * Get the age of the person
     *
     * @return the age of the person, in years
     */
    unsigned short getAge() const;


    /**
     * Get the list of children for the person
     *
     * @return the list of children for the person
     */
    const vector<Person>& getChildren() const;


    // operators

    /**
     * Overload the ostream << operator to allow printing info for the person
     *
     * Define << as a friend of the Person class to allow a Person object to be
     * written to an output stream.
     *
     * @param output the output stream to write the person's info to
     * @param person the Person to write the info for
     * @return the output stream
     */
    friend ostream& operator<< (ostream& output, const Person& person);


  private:

    // zero-argument constructor
    Person();

    // private attributes
    string _name;
    unsigned short _age;
    vector<Person> _children;

}; // Person

#endif // PERSON_H
