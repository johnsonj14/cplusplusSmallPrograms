/*
 * Program: personDB.cpp
 * Class: CSIS 370
 * Date: 2018-02-03
 */

#include "Person.h"
#include <iostream>

using namespace std;

/**
 * Adds a person to the persons vector
 *
 * @param persons is the person vector
 */
void addPerson(vector<Person>& persons)
{
    string name;
    short age;
    int i;
    bool alreadyExists = false;
    
    cout << "Enter the name > ";
    cin >> name;
    
    //iterates through persons to find the person by name
    i = 0;
    while (!alreadyExists && (i < persons.size()))
    {
        if (persons.at(i).getName() == name)
        {
            alreadyExists = true;
        }
        i++;
    }
    if (alreadyExists)
    {
        cerr << "Error! This person already exists." << endl;
    }
    else
    {
        cout << "Enter the age > ";
        cin >> age;
        persons.push_back(Person(name, age));
    }
}


/**
 * Finds a person in the persons vector
 *
 * @param persons is the person vector
 */
void findPerson(vector<Person>& persons)
{
    bool found = false;
    string nameToFind;
    int i;
    cout << "Enter the name to find > ";
    cin >> nameToFind;
    
    //iterates through persons to find the person by name
    i = 0;
    while (!found && (i < persons.size()))
    {
        if (persons.at(i).getName() == nameToFind)
        {
            persons.erase(persons.begin()+i);
            cout << persons.at(i) << endl;
            found = true;
        }
        i++;
    }
    
    // error message if the person couldn't be found
    if (found == false)
    {
        cerr << "Error! Person not found." << endl;
    }
}


/**
 * Deletes a person from the persons vector
 *
 * @param persons is the person vector
 */
void deletePerson(vector<Person>& persons)
{
    string nameToDelete;
    bool deleted = false;
    int i;
    cout << "Enter the name of the person to be deleted > ";
    cin >> nameToDelete;
    
    // iterates through persons to delete the person by name
    i = 0;
    while (!deleted && (i < persons.size()))
    {
        if (persons.at(i).getName() == nameToDelete)
        {
            persons.erase(persons.begin()+i);
            cout << nameToDelete << " has been deleted." << endl;
            deleted = true;
        }
        i++;
    }
    
    // error message if the person couldn't be found
    if (deleted == false)
    {
        cerr << "Error! Person not found." << endl;
    }
}


/**
 * Lists the people in the persons vector
 *
 * @param persons is the person vector
 */
void listPersons(vector<Person>& persons)
{
    for (auto& person: persons)
    {
        cout << person  << endl;
    }
}


/**
 * Runs a person database
 */
int main (int argc, char *argv[])
{
    // vector containing all of the persons
    vector<Person> persons;
    string menuInput;
    
    // while the user does not want to quit, keep running the menuInput
    while (menuInput != "q")
    {
        cout << "Add (a) - Find (f) - Delete (d) - List (l) - Quit (q)" << endl;
        cout << "Menu option > ";
        cin >> menuInput;
        
        // adds a person
        if (menuInput == "a")
        {
            addPerson(persons);
        }
        
        // finds a person and displays them
        else if (menuInput == "f")
        {
            findPerson(persons);
        }
        
        // deletes a person
        else if (menuInput == "d")
        {
            deletePerson(persons);
        }
        
        // displays a the persons
        else if (menuInput == "l")
        {
            listPersons(persons);
        }
        
        // quits
        else if (menuInput == "q")
        {
            persons.clear();
        }
        
        // if the menuInput wasn't a valid option
        else
        {
            cerr << "Error! Enter valid input!" << endl;
        }
    }
}
