/**
 * 1851268
 * Assignment 1
 * 2018-01-27
 *
 * stringReversal.cpp
 * stringReversal
 */

using namespace std;

#include <iostream>
#include <stack>
#include <string.h>

/**
 * Takes input from command line and reverses the character order.
 *
 * @param argc is the argument count
 * @param argv is an array of arguments
 */
int main(int argc, const char * argv[]) {
    
    // if command line doesn't recieve enough args
    if (argc == 1)
    {
        cout << "Insufficient Args" << endl;
        return 1;
    }
    else
    {
        // variables for the stack and character holder
        stack <char> Stack;
        char *chars = nullptr;
        
        // Iterates through the command line arguments after the program name
        // Pushes each character onto the Stack
        for (int i = 1; i < argc; ++i)
        {
            string arg = argv[i];
            chars = &arg[0u];
            
            // Pushing to stack
            for (int i = 0; i < strlen(chars); i++)
            {
                Stack.push(arg[i]);
            }
            // Adds spaces between words
            if (i < argc-1)
            {
                Stack.push(' ');
            }
        } // for
        
        // Pop all of the characters on the Stack to cout
        while (!Stack.empty())
        {
            cout << Stack.top();
            Stack.pop();
        }
        cout << endl;
    } //else
    
    return 0;
}
